document
  .getElementById('registrationForm')
  .addEventListener('submit', function (event) {
    event.preventDefault();
    let errorMessages = '';

    const fullName = document.getElementById('fullName').value;
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;
    const confirmPassword = document.getElementById('confirmPassword').value;
    const dob = document.getElementById('dob').value;
    const gender = document.getElementById('gender').value;

    if (!fullName) {
      errorMessages += 'Full Name is required.<br>';
    }
    if (!email) {
      errorMessages += 'Email Address is required.<br>';
    } else if (!validateEmail(email)) {
      errorMessages += 'Email Address is not valid.<br>';
    }
    if (!password) {
      errorMessages += 'Password is required.<br>';
    } else if (password.length < 6) {
      errorMessages += 'Password must be at least 6 characters long.<br>';
    }
    if (!confirmPassword) {
      errorMessages += 'Confirm Password is required.<br>';
    } else if (password !== confirmPassword) {
      errorMessages += 'Passwords do not match.<br>';
    }
    if (!dob) {
      errorMessages += 'Date of Birth is required.<br>';
    }
    if (!gender) {
      errorMessages += 'Gender is required.<br>';
    }

    document.getElementById('errorMessages').innerHTML = errorMessages;

    if (!errorMessages) {
      alert('Form submitted successfully!');
      // Here you would normally submit the form data to the server
    }
  });

function validateEmail(email) {
  const re = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
  return re.test(String(email).toLowerCase());
}
